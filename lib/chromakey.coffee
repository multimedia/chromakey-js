if typeof(module) is 'object'
  exports = module.exports
else
  exports = window

###
Parameters:
  input      the input canvas to estract chromakey background
  hueKey     the hue base value (on HSL colorspace), to remove a pixel
  hueDelta1  the hue base value distance to remove a pixel
  hueDelta2  the hue base value distance to reduce the pixel alpha
  hueDelta3  the hue base value distance to adjust the pixel hue
  maxLight   the maximum light to consider the pixel removal
  minLight   the minimal light to consider the pixel removal
  minSatur   the minimal saturation to consider the pixel removal
###

class exports.ChromaKey2Alpha
  constructor: (config={}) ->
    @input = config.input
    @hueKey    = config.hueKey    || 120
    @hueDelta1 = config.hueDelta1 || 30
    @hueDelta2 = config.hueDelta2 || 60
    @hueDelta3 = config.hueDelta3 || 70
    @maxLight  = config.maxLight  || 0.8
    @minLight  = config.minLight  || 0.2
    @minSatur  = config.minSatur  || 0.2
    @debugMode = null
    if config.debug is 'HUE_CORRECTION'
      @debugMode = 'HUE_CORRECTION'
      @rotateNearHueKey = @rotateNearHueKeyDebug
    for dMode in 'NO_HUE_CORRECTION HUE_TEST_ONLY LIGHT_TEST_ONLY DARK_TEST_ONLY GRAY_TEST_ONLY'.split ' '
      @debugMode = dMode if config.debug is dMode
    console.log "Debug mode is #{@debugMode}." if @debugMode

  removeChromaKey: ->
    ctx = @input.getContext '2d'
    img = ctx.getImageData 0, 0, @input.width, @input.height
    # img.data is an array for canvas representation with 4 itens per pixel.
    for r,i in img.data by 4
      r = r / 255
      g = img.data[i+1] / 255
      b = img.data[i+2] / 255
      [h,s,l] = rgb2hsl r,g,b
      a = if @removeInvalidPx(h,s,l) is 0 then 0 else
        Math.max @alphaNearHueKey(h,s,l), @alphaNearHueless(h,s,l)
      if @debugMode
        a = @alphaNearHueKey(h,s,l) if @debugMode is 'HUE_TEST_ONLY'
        a = @alphaNearLight(h,s,l)  if @debugMode is 'LIGHT_TEST_ONLY'
        a = @alphaNearDark(h,s,l)   if @debugMode is 'DARK_TEST_ONLY'
        a = @alphaNearGray(h,s,l)   if @debugMode is 'GRAY_TEST_ONLY'
      h = @rotateNearHueKey h,s,l unless @debugMode is 'NO_HUE_CORRECTION'
      [r,g,b] = hsl2rgb h,s,l
      img.data[i+0] = r*255
      img.data[i+1] = g*255
      img.data[i+2] = b*255
      img.data[i+3] = a*255
    ctx.putImageData img, 0, 0

  # Color Test
  validPx: (h,s,l)-> abs(h-@hueKey)>@hueDelta1 || l>@maxLight || l<@minLight || s<@minSatur
  invalidPx2alpha: (h,s,l)-> abs(h-@hueKey)<@hueDelta2 && l<@maxLight && l>@minLight
  nearHueKey: (h,s,l)-> abs(h-@hueKey)<@hueDelta3

  # Alpha calc
  removeInvalidPx: (h,s,l)->
    if @validPx(h,s,l) then 1 else 0
  alphaNearHueKey: (h,s,l)->
    #if @invalidPx2alpha(h,s,l) then (abs(h-@hueKey)-@hueDelta1)/(@hueDelta2-@hueDelta1) else 0
    (abs(h-@hueKey)-@hueDelta1)/(@hueDelta2-@hueDelta1)
  alphaNearDark: (h,s,l)->
    if l<@minLight then (abs(h-@hueKey)/@hueDelta2)+(1-l/@minLight)*2 else 0
  alphaNearLight: (h,s,l)->
    if l>@maxLight then (abs(h-@hueKey)/@hueDelta2)+(1-(1-l)/(1-@maxLight))*2 else 0
  alphaNearGray: (h,s,l)->
    if s<@minSatur then (abs(h-@hueKey)/@hueDelta2)+(1-s/@minSatur)*2 else 0
  alphaNearHueless: (h,s,l)->
    grayAlpha = @alphaNearGray(h,s,l)
    if grayAlpha is 1
      grayAlpha
    else
      Math.max @alphaNearDark(h,s,l), @alphaNearLight(h,s,l), grayAlpha

  # Color correction
  keyDist: (h,s,l)->
    d = (h-@hueKey) / @hueDelta3
    sign(d) * (1-abs(d))
  curveKeyDist: (h,s,l)->
    d = @keyDist(h,s,l)
    sign(d)*d*d
  rotateNearHueKey: (h,s,l)->
    if @nearHueKey(h,s,l) then h+(@hueDelta3*@curveKeyDist(h,s,l)) else h
  rotateNearHueKeyDebug: (h,s,l)->
    if @nearHueKey(h,s,l) then 0 else 180

# Helpers

abs = Math.abs

sign = Math.sign || (n)-> if n<0 then -1 else 1

# Receive values between 0 and 1. 
# Return values a array [h, s, v], where:
# * h is 0..360
# * s is 0..1
# * v is 0..1
exports.rgb2hsl = rgb2hsl = (r,g,b)->
  max = Math.max r,g,b
  min = Math.min r,g,b
  c = max - min
  hi = 0
  unless c is 0
    hi = (g-b)/c % 6 if max is r
    hi = (b-r)/c + 2 if max is g
    hi = (r-g)/c + 4 if max is b
  h = hi * 60
  h = 360 + h if h < 0
  l = (max + min) / 2
  s = if l in [0,1] then 0 else c/( 1-abs(2*l-1) )
  [ h, s, l ]

exports.rgb2hsLuma = rgb2hsLuma = (r,g,b)->
  #TODO...
  luma = 0.30*r + 0.59*g + 0.11*b

# Receive values as exported by rgb2hsl.
# Return values a array [r, g, b], where values are between 0 and 1.
exports.hsl2rgb = hsl2rgb = (h,s,l)->
  c = ( 1-abs(2*l-1) ) * s
  hi = h/60;
  x = c * ( 1-abs(hi%2-1) )
  if      hi<1 then r=c; g=x; b=0
  else if hi<2 then r=x; g=c; b=0
  else if hi<3 then r=0; g=c; b=x
  else if hi<4 then r=0; g=x; b=c
  else if hi<5 then r=x; g=0; b=c
  else              r=c; g= 0; b=x
  m = l - c/2
  [ r+m, g+m, b+m ]
