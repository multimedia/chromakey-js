assert = require 'assert'
{rgb2hsl, hsl2rgb} = require '..'

demiRound = (values)->
  r = (val)-> Math.round(val*100)/100
  [ r(values[0]), r(values[1]), r(values[2]) ]

describe 'Color definition translation', ->

  it 'convert rgb to hsl at max saturation and medium luminosity', ->
    assert.deepEqual rgb2hsl(1.0, 0.0, 0.0), [  0, 1, 0.5], 'red'
    assert.deepEqual rgb2hsl(1.0, 0.5, 0.0), [ 30, 1, 0.5], 'orange'
    assert.deepEqual rgb2hsl(1.0, 1.0, 0.0), [ 60, 1, 0.5], 'yellow'
    assert.deepEqual rgb2hsl(0.5, 1.0, 0.0), [ 90, 1, 0.5], 'lime'
    assert.deepEqual rgb2hsl(0.0, 1.0, 0.0), [120, 1, 0.5], 'green'
    assert.deepEqual rgb2hsl(0.0, 1.0, 0.5), [150, 1, 0.5], 'spring'
    assert.deepEqual rgb2hsl(0.0, 1.0, 1.0), [180, 1, 0.5], 'cian'
    assert.deepEqual rgb2hsl(0.0, 0.5, 1.0), [210, 1, 0.5], 'sea'
    assert.deepEqual rgb2hsl(0.0, 0.0, 1.0), [240, 1, 0.5], 'blue'
    assert.deepEqual rgb2hsl(0.5, 0.0, 1.0), [270, 1, 0.5], 'violet'
    assert.deepEqual rgb2hsl(1.0, 0.0, 1.0), [300, 1, 0.5], 'purple'
    assert.deepEqual rgb2hsl(1.0, 0.0, 0.5), [330, 1, 0.5], 'pink'

  it 'convert hsl to rgb at max saturation and medium luminosity', ->
    assert.deepEqual hsl2rgb(  0, 1, 0.5), [1.0, 0.0, 0.0], 'red'
    assert.deepEqual hsl2rgb( 30, 1, 0.5), [1.0, 0.5, 0.0], 'orange'
    assert.deepEqual hsl2rgb( 60, 1, 0.5), [1.0, 1.0, 0.0], 'yellow'
    assert.deepEqual hsl2rgb( 90, 1, 0.5), [0.5, 1.0, 0.0], 'lime'
    assert.deepEqual hsl2rgb(120, 1, 0.5), [0.0, 1.0, 0.0], 'green'
    assert.deepEqual hsl2rgb(150, 1, 0.5), [0.0, 1.0, 0.5], 'spring'
    assert.deepEqual hsl2rgb(180, 1, 0.5), [0.0, 1.0, 1.0], 'cian'
    assert.deepEqual hsl2rgb(210, 1, 0.5), [0.0, 0.5, 1.0], 'sea'
    assert.deepEqual hsl2rgb(240, 1, 0.5), [0.0, 0.0, 1.0], 'blue'
    assert.deepEqual hsl2rgb(270, 1, 0.5), [0.5, 0.0, 1.0], 'violet'
    assert.deepEqual hsl2rgb(300, 1, 0.5), [1.0, 0.0, 1.0], 'purple'
    assert.deepEqual hsl2rgb(330, 1, 0.5), [1.0, 0.0, 0.5], 'pink'

  it 'convert saturation variation', ->
    assert.deepEqual demiRound(rgb2hsl 0.5, 0.5, 0.5), [0, 0.0, 0.5]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.0, 0.5), [0.5, 0.5, 0.5]
    assert.deepEqual demiRound(rgb2hsl 0.6, 0.4, 0.4), [0, 0.2, 0.5]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.2, 0.5), [0.6, 0.4, 0.4]
    assert.deepEqual demiRound(rgb2hsl 0.7, 0.3, 0.3), [0, 0.4, 0.5]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.4, 0.5), [0.7, 0.3, 0.3]
    assert.deepEqual demiRound(rgb2hsl 0.8, 0.2, 0.2), [0, 0.6, 0.5]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.6, 0.5), [0.8, 0.2, 0.2]
    assert.deepEqual demiRound(rgb2hsl 0.9, 0.1, 0.1), [0, 0.8, 0.5]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.8, 0.5), [0.9, 0.1, 0.1]
    assert.deepEqual demiRound(rgb2hsl 1.0, 0.0, 0.0), [0, 1.0, 0.5]
    assert.deepEqual                  demiRound(hsl2rgb 0, 1.0, 0.5), [1.0, 0.0, 0.0]

  it 'convert luminosity variation', ->
    assert.deepEqual demiRound(rgb2hsl 0.0, 0.0, 0.0), [0, 0.0, 0.0]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.0, 0.0), [0.0, 0.0, 0.0]
    assert.deepEqual demiRound(rgb2hsl 0.4, 0.0, 0.0), [0, 1.0, 0.2]
    assert.deepEqual                  demiRound(hsl2rgb 0, 1.0, 0.2), [0.4, 0.0, 0.0]
    assert.deepEqual demiRound(rgb2hsl 0.8, 0.0, 0.0), [0, 1.0, 0.4]
    assert.deepEqual                  demiRound(hsl2rgb 0, 1.0, 0.4), [0.8, 0.0, 0.0]
    assert.deepEqual demiRound(rgb2hsl 1.0, 0.2, 0.2), [0, 1.0, 0.6]
    assert.deepEqual                  demiRound(hsl2rgb 0, 1.0, 0.6), [1.0, 0.2, 0.2]
    assert.deepEqual demiRound(rgb2hsl 1.0, 0.6, 0.6), [0, 1.0, 0.8]
    assert.deepEqual                  demiRound(hsl2rgb 0, 1.0, 0.8), [1.0, 0.6, 0.6]
    assert.deepEqual demiRound(rgb2hsl 1.0, 1.0, 1.0), [0, 0.0, 1.0]
    assert.deepEqual                  demiRound(hsl2rgb 0, 0.0, 1.0), [1.0, 1.0, 1.0]

