curImg = null

$.ajax 'images/images.json',
  dataType: 'json',
  error: (jqXHR, textStatus, error)-> alert "Sorry...\nCould't load images.\n#{error}"
  success: (data, textStatus, jqXHR)->
    for img in data
      $('<img>').attr('src', 'images/' + img.file)
                .appendTo('#images')
                .click(-> curImg = this; do updateImg)
    do $('#images img')[0].click

applyImg = ->
  c = $('#canvas-area canvas')[0]
  c.width = curImg.naturalWidth
  c.height = curImg.naturalHeight
  ctx = c.getContext '2d'
  ctx.drawImage curImg, 0, 0

window.updateImg = ->
  if curImg? then do applyImg else return
  c2a = new ChromaKey2Alpha
          input: $('#canvas-area canvas')[0]
          hueKey: $('#hueKeyF').val()
          hueDelta1: $('#keyRay1F').val()
          hueDelta2: $('#keyRay2F').val()
          hueDelta3: $('#keyRay3F').val()
          maxLight: $('#maxLightF').val()
          minLight: $('#minLightF').val()
          minSatur: $('#minSaturF').val()
          debug: (i.value for i in $('#ctrlDebug input') when i.checked)[0]
  console.log c2a
  start = do Date.now
  do c2a.removeChromaKey
  end = do Date.now
  lapsedTime.innerHTML = ((end-start)/1000) + 'ms'

$('.slide360').slider
  min: 0, max: 360,
  change: (event, ui)->
    id = ui.handle.parentNode.id[..-2] + 'F'
    $('#'+id).val ui.value
    do updateImg

$('.slideOne').slider
  min: 0, max: 1, step:0.01
  change: (event, ui)->
    id = ui.handle.parentNode.id[..-2] + 'F'
    $('#'+id).val ui.value
    do updateImg

$('#ctrlSliders input').on 'change', ->
  id = @id[..-2] + 'G'
  @value = $('#'+id).slider('value', @value).slider 'value'

for id,value of {
    hueKey:120, keyRay1:30, keyRay2:60, keyRay3:70,
    maxLight:0.8, minLight:0.2, minSatur:0.2
  }
  $("##{id}F").val value
  $("##{id}F").trigger 'change'

$('#ctrlDebug input').on 'click', -> do updateImg

do window.openCtrl = ->
  do $('#open-ctrl').hide
  $('#ctrl').dialog (
    width: 635
    close: -> do $('#open-ctrl').show
  )

